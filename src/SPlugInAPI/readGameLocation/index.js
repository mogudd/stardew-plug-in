import regedit from 'regedit'
const path = 'HKLM\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Steam App 413150'


export default function readGameLocation() {
    return new Promise((resolve, reject) => {
        regedit.list(path, (err, result) => {
            if (result) {
                resolve(result[path].values.InstallLocation.value)
                return;
            }
            reject(reject)
        })
    })
}