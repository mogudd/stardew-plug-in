import regedit from 'regedit'
import path from 'path'
//方式1：通过注册表读取安装目录

const regeditPath = 'HKLM\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Steam App 413150'
// eslint-disable-next-line no-undef
const fileLocation = path.join(__static, 'vbs')
regedit.setExternalVBSLocation(fileLocation);

export default function readGameLocation() {
    return new Promise((resolve, reject) => {
        regedit.list(regeditPath, (err, result) => {
            if (result) {
                resolve(result[regeditPath].values.InstallLocation.value)
                return;
            }
            reject(reject)
        })
    })
}

//方式2：通过steam客户端安装位置寻找星露谷物语安装目录