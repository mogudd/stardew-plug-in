const watch = require('@atom/watcher')
const path = require('path')
// const getModNameFromPath = require('./getModNameFromPath')

//test
// ModObject
/* TESTCODE*/
// let manifest = {
//     "Name": "Console Commands",
//     "Author": "SMAPI",
//     "Version": "3.8.1",
//     "Description": "Adds SMAPI console commands that let you manipulate the game.",
//     "UniqueID": "SMAPI.ConsoleCommands",
//     "EntryDll": "ConsoleCommands.dll",
//     "MinimumApiVersion": "3.8.1"
// }

// let ModObjectInstance = new ModObject("D:\\steam\\steamapps\\common\\Stardew Valley\\Mods\\ConsoleCommands",manifest)
// let ModObjects = new Map()
// ModObjects.set(ModObjectInstance.Path, ModObjectInstance)
let ModObjects = global.ModObjectsMap
// global.gameLocation = `${global.gameLocation}/Mods` || "D:/steam/steamapps/common/Stardew Valley/Mods"


async function watcher(root, options = {}, callbacks = {}) {
    return await watch.watchPath(root, options, callbacks);
}


watcher(global.gameLocation, {}, events => {
    for (const event of events) {
        switch (event.action) {
            case 'created':

                break
            case 'modified':

                break

            case 'deleted':
                deletedEvent(event)
                break

            case 'renamed':
                renamedEvent(event)
                break
        }
    }
})

function renamedEvent(event) {
    if ((event.kind === 'directory') &&
        (event.oldPath !== global.gameLocation) &&
        (path.resolve(event.oldPath, '..') === global.gameLocation)) {
        //Mod目录磁盘名称更改
        //通知模型更改
        let mod = ModObjects.get(event.oldPath)
        mod.Path = event.Path
    }
}

function deletedEvent(event) {
    if ((event.kind === 'directory') && 
        (event.Path !== global.gameLocation)) {
        //通知模型更改
        // let mod = ModObjects.get(event.oldPath)
        global.appg.webContents.send("ModObjectDelete",event.Path)
        ModObjects.delete(event.Path)

    }
}