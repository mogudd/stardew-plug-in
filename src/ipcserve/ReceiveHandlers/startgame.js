const {
    ipcMain
} = require('electron')
const {
    exec
} = require('child_process')
const iconvLite = require('iconv-lite');

function startgame() {
    
    //node不支持gbk编码，而cmd默认是gbk编码所以需要处理一下
    const gameprocess = exec(`"${global.gameLocation}/StardewModdingAPI.exe"`, {
        cwd: global.gameLocation,
        encoding: "binary"
    })
    let count = 0
    gameprocess.stdout.on('data', (data) =>{
        console.log("This is the :" + count++,iconvLite.decode(data,'cp936'))
    })
    // gameprocess.pid
}

ipcMain.on("StartGame", () => {
    console.log("Start")
    startgame()
})

// module.exports = startgame