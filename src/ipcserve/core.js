/* eslint-disable no-empty */
//监听渲染进程需要调用的API
const {
    ipcMain
} = require('electron')
const fs = require('fs/promises')
const path = require('path')
// const IpcInvokeEvent = require('./IpcInvokeEvent')
async function ipcEventHandler() {
    callingNativeApi()
    autoRequireModules()
}

function callingNativeApi() {
    ipcMain.handle('NativeAPI', async (event, apiname, ...args) => {
        try {
            var value = await require(`@/SPlugInAPI/${apiname}/index.js`)(...args)
            return value
        } catch (e) {
            console.log(e)
        }
    })
}
async function autoRequireModules() {
//动态载入ipcserve
    const requireHandler = require.context('./',true, /\.js$/)

    let filekeys = requireHandler.keys()
    filekeys.forEach(key => {
        if(key.search("core") == -1 && key.search(".js") != -1) {
           let handler = requireHandler(key)
           console.log(handler)
        }
    })
}


module.exports = ipcEventHandler