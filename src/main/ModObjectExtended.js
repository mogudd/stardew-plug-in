const ModObject = require("../common/ModObject")
const fs = require('fs/promises');
const path = require('path');

class ModObjectMain extends ModObject {
    constructor(path, manifest) {
        super(path, manifest)
    }

    set Path(value) {
        global.appg.webContents.send("ModObjectChangePath", this._Path, value)
        this._Path = value
    }
    set Manifest(value) {
        global.appg.webContents.send("ModObjectChangeMani", this._Path, value)
        this._Manifest = value
    }
    set Name(value) {
        global.appg.webContents.send("ModObjectChangeName", this._Name, value)
        this._Name = value
    }

    uninstallMod() {
        //将MOD文件夹最前面加上 '.' 这样的文件会被SMAPI认为是不需要加载的MOD
        let parsedpath = path.parse(this.Path)
        if (parsedpath.name[0] != '.') {
            parsedpath.base = '.' + parsedpath.base
            fs.rename(this.Path, path.format(parsedpath)).catch(err => console.error('renameError', err))
        }

        //renamedir opreate
    }
    installMod() {
        let parsedpath = path.parse(this.Path)
        if (parsedpath.name[0] == '.') {
            parsedpath.base = parsedpath.base.splice(0, 1)
            fs.rename(this.Path, path.format(parsedpath)).catch(err => console.error('renameError', err))
        }
        //将MOD文件夹最前面的 '.' 去除
    }
    removeMod() {
        //从硬盘中删除 MOD 文件夹
    }
}

module.exports = ModObjectMain