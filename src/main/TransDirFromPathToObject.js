const scanningDir = require('./scanningModDir')
const ModObject = require('./ModObjectExtended')
const fs = require('fs/promises')
const path = require('path')
//test
// function scanningDir(){
//     return new Promise((resolve, reject) => {
//         resolve(['D:/steam/steamapps/common/Stardew Valley/Mods/SimpleCropLabel'])
//     })
// }
async function readManifestFile(_path) {
    try {
        let filebuffer = await fs.readFile(path.join(_path, 'manifest.json'))
        // console.log(JSON.parse(filebuffer.toString()))
        return JSON.parse(filebuffer.toString())
    } catch (e) {
        if (e.code === 'ENOENT') console.error("无效的Mod文件夹,没有在以下文件夹中找到manifest.json文件", e.path)
        else console.error(e)
    }

}

async function transModObjects() {                          
    let ModObjects = new Map();
    let dirs = await scanningDir()
    let taskes = dirs.map(async (path) => {
        let filedata = await readManifestFile(path)
        if (filedata) ModObjects.set(path,new ModObject(path,filedata))
    })
    await Promise.all(taskes)
    return ModObjects
}

// creatModObjects().then(value => console.log(value))
module.exports = transModObjects
