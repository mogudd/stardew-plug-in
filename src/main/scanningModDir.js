const fs = require('fs')
const path = require('path')
let dirs = []   //resolve值
const rootpath = (global.gameLocation || "D:\\steam\\steamapps\\common\\Stardew Valley") + '/Mods'

module.exports = function scanningDir() {

    return new Promise((resolve, reject) => {
        fs.readdir(rootpath, {
            withFileTypes: true
        }, (err, files = []) => {
            console.error(err)
            console.log(files)
            let iterator = 0;
            files.forEach(value => {
                if (!value) return;
                let dirpath = path.join(rootpath, value.name)
                fs.stat(dirpath, (err, status) => {
                    if (err) reject(err)
                    if (status.isDirectory()) {
                        dirs.push(dirpath)
                    }
                    iterator++
                    if (iterator === files.length) resolve(dirs)
                })
            })
        })
    })
}