import {
  createRouter,
  createWebHashHistory
} from 'vue-router'


const routes = [{
    path: '/',
    name: 'Initial',
    component: () => import('../views/Initialize.vue')
  },
  {
    path: '/initialized',
    name: 'App',
    component: () => import('../views/Initialized.vue'),
    children:[  {
      path: 'home',
      name: 'Home',
      component: () => import('../views/Home.vue')
    },
    {
      path: 'about',
      name: 'About',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/About.vue')
    }]
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router