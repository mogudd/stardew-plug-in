class ModObject {
    constructor(path, manifest) {
        this._Path = path
        this._Manifest = manifest
        this._Name = manifest.Name
    }
    get Path() { return this._Path }
    get Manifest() { return this._Manifest }
    get Name() { return this._Name }
}
module.exports = ModObject