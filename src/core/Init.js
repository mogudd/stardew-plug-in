// import ipcEventHandler from '../ipcserve/core.js'
const readGameLocation = require('../uilts/readGameLocation.js').default
const ipcEventHandler = require('../ipcserve/core.js')
const loadedModObjects = require('../main/TransDirFromPathToObject')
//初始化node本地模块使用
// console.log(loadedModObjects)
async function initializeCore() {
    //初始化本地调用
    //在全局挂载以下方法/属性
    //global.appg   (在background.js中已经预先挂载)
    //global.gameLocation
    //global.ModObjectsMap
    try {
        global.gameLocation = await readGameLocation()
        await Promise.all([ipcEventHandler(),global.ModObjectsMap = loadedModObjects()])
        
    } catch (e) {
        console.error(e)
    }
    console.log("completed")
    setTimeout(() =>{global.appg.webContents.send("initialedCompleted",'QAQ');console.log(global.appg)},3000)
    // global.appg.webContents.send("initialedCompleted",'QAQ')
    
    // console.log(global.appg.webContents)
}
export default initializeCore