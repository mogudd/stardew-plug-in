module.exports = {
  purge: { content: ['./public/**/*.html', './src/**/*.vue'] },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      // #55EFC4
      boxShadow:{
        "lg-green": '0px 0px 15px -3px rgba(85, 239, 196, 0.85), 0 0px 15px -3px rgba(85, 239, 196, 0.85)',
        "lg-orange": '0px 0px 15px -3px #FFC66B, 0 0px 15px -3px #FFC66B',
        "lg-gray": '0px 0px 15px -3px #DFE6E9, 0 0px 15px -3px #DFE6E9',
        "lg-red": '0px 0px 15px -3px #FCA5A5, 0 0px 15px -3px #FCA5A5',
      },
      backgroundColor:{
        "green": '#55EFC4',
        "orange": '#FFC66B'    
        },
        zIndex: {
          '-10': '-10',
          '-1':'-1'
         }
    },

  },
  variants: {
    extend: {
      boxShadow:['hover'],
      animation:['hover']
    },
  },
  plugins: [],
}
