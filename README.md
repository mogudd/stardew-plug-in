# StardewPlugIn

## 📄 项目介绍
这是一个游戏《星露谷物语》[MOD加载器](https://smapi.io/)的 UI 为了方便玩家对模组进行管理，所以开发了这个项目。这个项目实际上可以派生出很多游戏的模组管理系统，并不单只是一个游戏的。

## 🎞 项目预览
![项目](./src/assets/ProjectCap.jpg)

## 👇 开发技术栈
 - [Electron](https://www.electronjs.org/)(v12)
 - Vue(v3)
 - Vue-Router(v4)
 - Tailwind(v2.1)

## ✍ TODO LIST
 - [ ] 美观易用的UI
 - [ ] 模组管理(安装、卸载、更新)
 - [ ] 在线模组下载
 - [ ] SMAPI 版本管理
 - [ ] 《星露谷物语》版本管理
 - [ ] 远程联机

## ❓开发注意事项
 - 使用 ffi-napi 调用了系统 API 所以请注意开发环境中有以下软件
    - Python 2.7, 3.5, 3.6, 3.7, 3.8, 3.9
    - Windows 平台
        windows-build-tools
    - Mac 平台
        Xcode
    - Linux or unix
        - make
        - c/c++ 编译工具，如GCC

A:ELECTRON_MIRROR="https://cdn.npm.taobao.org/dist/electron/" npm install electron
## 附
瞎开发玩的，暂时不能使用QwQ。。。。

